package com.springapp.mvc;
import org.apache.log4j.Logger;

import javax.persistence.*;
/**
 * Created by ������� on 02.04.2015.
 */@Entity(name = "account")
public class User {
    private static final Logger log = Logger.getLogger(User.class);
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Basic
    private String Name;

    @Basic
    private String NickName;

    @Basic
    private String email;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return Name;
    }

    public void setFirstName(String name) {
        this.Name = name;
    }

    public String getLastName() {
        return NickName;
    }

    public void setLastName(String lastName) {
        this.NickName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
