package com.springapp.mvc.controller;

/**
 * Created by ������� on 05.04.2015.
 */
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
@Controller
@RequestMapping("/")
public class MainCnrtl {
    @RequestMapping(method = RequestMethod.GET)
    public String start(Model model){
        return "index";
    }
}
