package com.springapp.mvc.entity.enums;

/**
 * Created by ������� on 05.04.2015.
 */
public enum UserRoleEnum {
    ADMIN,
    USER,
    ANONYMOUS;

    UserRoleEnum() {
    }
}
