package com.springapp.mvc.entity;
import org.apache.log4j.Logger;

import javax.persistence.*;
/**
 * Created by ������� on 02.04.2015.
 */@Entity(name = "account")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Basic
    private String Name;

    @Basic
    private String Passworld;

    @Basic
    private String email;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getPassworld() {
        return Passworld;
    }

    public void setPassworld(String passworld) {
        this.Passworld = Passworld;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
