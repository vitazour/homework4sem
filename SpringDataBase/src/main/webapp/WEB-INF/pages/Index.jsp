<%--
  Created by IntelliJ IDEA.
  User: Виталий
  Date: 05.04.2015
  Time: 19:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>News accelerator</title>
    <link href="<c:url value="css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="css/jumbtron-narrow.css" />" rel="stylesheet">
</head>
<body>
<div class="container">

    <div class="jumbotron" style="margin-top: 20px;">
        <h1>News Accelerator</h1>

        <p class="lead">
            News Accelerator - это сервис по нахождению интересующих вас новстей.
        </p>
        <sec:authorize access="!isAuthenticated()">
            <p><a class="btn btn-lg btn-success" href="<c:url value="/login" />" role="button">Войти</a></p>
        </sec:authorize>
        <sec:authorize access="isAuthenticated()">
            <p>Ваш логин: <sec:authentication property="principal.username"/></p>

            <p><a class="btn btn-lg btn-danger" href="<c:url value="/logout" />" role="button">Выйти</a></p>

        </sec:authorize>
    </div>

    <div class="footer">
        <p>© Виталий Абрамов 2015</p>
    </div>

</div>
</body>
</html>
